The S2-0004 DISPENSER D-X30 has the following technical specifications.

## Technical information
|Parameter|Value|
|:----|----:|
|Abmessungen in mm (B x T x H)|49 x 292 x 341|
|Gewicht in kg|4,8|
|Verfahrbereich in Z in mm|150|
|Kartuschenaufnahme EFD|3 cc, 5 cc, 10 cc, 30 cc|
|Kartuschenaufnahme SEMCO|5 cc, 10 cc, 30 cc|
|Min. Dosiervolumen in µl|0,01|
|Max. Temperatur in °C|130|
|Spannung in V|24|
|Max. Stromstärke in A|4|
|Kommunikationsschnittstelle|UNICAN|

