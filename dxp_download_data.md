Here, you will find an overview of the open source informmation of this product. The detailed information can be found within the repository at [GitLab](https://gitlab.com/ourplant.net/products/s2-0004_dispenser_d-x30).

|document|download options|
|:-----|-----:|
|operating manual           |[de](https://gitlab.com/ourplant.net/products/S2-0004-dispenser-d-x30/-/raw/main/01_Operating_Manual/S2-0004_B1_Betriebsanleitung.pdf); [en](https://gitlab.com/-/ide/project/ourplant.net/products/s2-0004-dispenser-d-x30/tree/main/-/01_Operating_Manual/S2-0004_A1_Operating%20Manual.pdf) |
|assembly drawing           |[de](https://gitlab.com/ourplant.net/products/S2-0004-dispenser-d-x30/-/raw/main/09_assembly_drawing/s2-0004_ZNB_E_Dispenser_D-X30.pdf)|
|circuit diagram            |[de](https://gitlab.com/ourplant.net/products/S2-0004-dispenser-d-x30/-/raw/main/02_Schaltplan/S2-0004-EPLAN-D.pdf)|
|maintenance instructions   ||
|spare parts                |[de](https://gitlab.com/ourplant.net/products/S2-0004-dispenser-d-x30/-/raw/main/04_spare_parts/S2-0004_C2_EVL.pdf)|

<!-- 2021 (C) Häcker Automation GmbH -->
